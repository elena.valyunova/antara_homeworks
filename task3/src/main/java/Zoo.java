import animal.Animal;
import animal.Aviary;
import animal.Interfaces.Swim;
import animal.Interfaces.Voice;
import animal.carnivorous.*;
import animal.herbivore.*;
import food.Food;
import food.Grass;
import food.Meat;

import static animal.SizeAviary.*;

public class Zoo {
    public static void main(String[] args) throws WrongFoodException {

        Herbivore lama = new Lama("Alisa", 102.7, 5, THREE);
        Herbivore giraffe = new Giraffe("Melman", 1000, 14, FOUR);
        Herbivore duck = new Duck("Kryakva", 1.1, 6, ONE);
        Herbivore lion = new Lion("Alex", 150, 7, THREE);
        Carnivorous carakal = new Carakal("BigFloppa", 15, 10, TWO);
        Carnivorous eagle = new Eagle("MightyEagle", 6.2, 16, TWO);
        Carnivorous fish = new Fish("Bruce", 770, 8, ONE);
        Carnivorous manul = new Manul("ElGato", 2.9, 7, TWO);
        Herbivore swanGirl = new Duck("Daisy", 8.5, 8, TWO);
        Herbivore swanBoy = new Duck("Dewey", 10, 9, TWO);

        lama.getAll();
        giraffe.getAll();
        duck.getAll();
        lion.getAll();
        carakal.getAll();
        eagle.getAll();
        fish.getAll();
        manul.getAll();
        swanBoy.getAll();
        swanGirl.getAll();

        Food meat = new Meat("meat");
        Food grass = new Grass("grass");

        Worker worker = new Worker("Mike");
        //Вылетит исключение:
        //worker.feed(manul,grass);
        worker.feed(manul, meat);
        worker.getVoice((Voice) lion);
        //Выдаст ошибку:
        //worker.getVoice((Voice) fish);

        Swim[] pond = new Swim[3];
        pond[0] = (Swim) duck;
        pond[1] = (Swim) swanBoy;
        pond[2] = (Swim) swanGirl;

        for (Swim swim : pond) {
            swim.swim();
        }

        Aviary<Carnivorous> veryLittleAviaryCarnivorous = new Aviary<>(ONE);
        Aviary<Carnivorous> littleAviaryCarnivorous = new Aviary<>(TWO);
        Aviary<Carnivorous> middleAviaryCarnivorous = new Aviary<>(THREE);
        Aviary<Carnivorous> bigAviaryCarnivorous = new Aviary<>(FOUR);

        Aviary<Herbivore> veryLittleAviaryHerbivore = new Aviary<>(ONE);
        Aviary<Herbivore> littleAviaryHerbivore = new Aviary<>(TWO);
        Aviary<Herbivore> middleAviaryHerbivore = new Aviary<>(THREE);
        Aviary<Herbivore> bigAviaryHerbivore = new Aviary<>(FOUR);

        bigAviaryHerbivore.addAnimal(giraffe);
        middleAviaryHerbivore.addAnimal(lama);

        bigAviaryHerbivore.deleteAnimal(lama);

        Animal result = bigAviaryHerbivore.getLink("Melman");
        System.out.println(result);

    }
}
