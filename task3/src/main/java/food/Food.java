package food;

public abstract class Food {
    protected String title;

    public Food(String title){
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
