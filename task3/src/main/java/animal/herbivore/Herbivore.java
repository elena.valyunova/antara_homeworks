package animal.herbivore;

import animal.Animal;
import animal.SizeAviary;
import food.Food;
import food.Grass;


public abstract class Herbivore extends Animal {
    public Herbivore(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public boolean eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            System.out.println("Кушает");
            return true;
        } else throw new WrongFoodException("Нельзя кормить травоядных мясом!");
    }
}
