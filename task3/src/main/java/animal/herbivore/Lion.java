package animal.herbivore;

import animal.SizeAviary;
import animal.Interfaces.Run;
import animal.Interfaces.Voice;

public class Lion extends Herbivore implements Voice, Run {

    public Lion(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public String voice() {
        return "Это мой выбор - быть вегетарианцем";
    }


    public void run() {
        System.out.println("Лев бежит");
    }
}
