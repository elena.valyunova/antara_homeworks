package animal.herbivore;

import animal.SizeAviary;
import animal.Interfaces.Run;

public class Lama extends Herbivore implements Run {

    public Lama(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public void run() {
        System.out.println("Лама бежит");
    }

}
