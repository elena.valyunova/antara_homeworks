package animal.herbivore;

import animal.SizeAviary;
import animal.Interfaces.Run;

public class Giraffe extends Herbivore implements Run {

    public Giraffe(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public void run() {
        System.out.println("Жираф бежит");
    }

}
