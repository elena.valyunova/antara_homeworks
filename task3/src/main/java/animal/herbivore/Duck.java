package animal.herbivore;

import animal.SizeAviary;
import animal.Interfaces.Fly;
import animal.Interfaces.Swim;
import animal.Interfaces.Voice;

public class Duck extends Herbivore implements Fly, Swim, Voice {

    public Duck(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }


    public void fly() {
        System.out.println("Уточка полетела");
    }

    public void swim() {
        System.out.println("Уточка поплыла");
    }

    public String voice() {
        return "Кря-кря";
    }
}
