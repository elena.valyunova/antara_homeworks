package animal;

import animal.herbivore.WrongFoodException;
import food.Food;
import food.Grass;

public abstract class Animal {

    protected String name;
    protected double weight;
    protected int age;
    protected SizeAviary size;

    public Animal(String name, double weight, int age, SizeAviary size) {
        this.name = name;
        this.weight = weight;
        this.age = age;
        this.size = size;
    }

    public abstract boolean eat(Food food) throws WrongFoodException;

    public void getAll() {
        System.out.println(getName() + ", весит " + getWeight() + " кг и ему " + getAge() + " лет.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public SizeAviary getSize() {
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        if (this == o) return true;
        Animal animal = (Animal) o;
        return name.equals(animal.getName());
    }

    @Override
    public int hashCode() {
        int result = name == null ? 0 : name.hashCode();
        result = 29 * result + (int) weight;
        result = 29 * result + age;
        return result;
    }
}
