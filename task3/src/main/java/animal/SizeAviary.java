package animal;

public enum SizeAviary {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4);

    private final int i;

    SizeAviary(int i) {
        this.i = i;
    }

    public int getLevelCode() {
        return this.i;
    }
}
