package animal.carnivorous;

import animal.SizeAviary;
import animal.Interfaces.Swim;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public void swim() {
        System.out.println("Рыбка поплыла");
    }
}
