package animal.carnivorous;

import animal.SizeAviary;
import animal.Interfaces.Fly;
import animal.Interfaces.Voice;

public class Eagle extends Carnivorous implements Fly, Voice {

    public Eagle(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public String voice() {
        return "Кли-кли-кли-кли";
    }

    public void fly() {
        System.out.println("Орел полетел");
    }

}
