package animal.carnivorous;

import animal.SizeAviary;
import animal.Animal;
import animal.herbivore.WrongFoodException;
import food.Food;
import food.Grass;
import food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public boolean eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            System.out.println("Кушает");
            return true;
        } else throw new WrongFoodException("Нельзя кормить травоядных мясом!");
    }
}
