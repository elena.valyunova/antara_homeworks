package animal.carnivorous;

import animal.SizeAviary;
import animal.Interfaces.Run;
import animal.Interfaces.Voice;

public class Manul extends Carnivorous implements Run, Voice {

    public Manul(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public String voice() {
        return "Мяу-мяу";
    }

    public void run() {
        System.out.println("Манул побежал");
    }
}
