package animal.carnivorous;

import animal.SizeAviary;
import animal.Interfaces.Run;
import animal.Interfaces.Voice;

public class Carakal extends Carnivorous implements Run, Voice {


    public Carakal(String name, double weight, int age, SizeAviary size) {
        super(name, weight, age, size);
    }

    public String voice() {
        return "Мя мя";
    }

    public void run() {
        System.out.println("Каракал побежал");
    }

}
