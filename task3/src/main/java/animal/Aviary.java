package animal;

//Создать обобщенный (generic) класс вольера для животных из предыдущего здания.
//Должна быть возможность создавать вольеры отдельно для хищников и травоядных.

import java.util.HashSet;

public class Aviary<T> {
    SizeAviary sizeOfAnimal;
    private HashSet<T> aviary = new HashSet<>();

    public Aviary(SizeAviary sizeOfAnimal) {
        this.sizeOfAnimal = sizeOfAnimal;
    }

    public void addAnimal(T animal) {
        // добавить животное в вольер (метод принимает объект животного с соответствующим вольеру типом)
        SizeAviary size = ((Animal) animal).getSize();
        if (size == sizeOfAnimal || size.getLevelCode() < sizeOfAnimal.getLevelCode()) aviary.add(animal);

    }

    public void deleteAnimal(T animal) {
        //удалить животное из вольера
        aviary.remove(animal);
    }

    public T getLink(String name) {
        //получить ссылку на животное в вольере по его кличке
        //(или другому полю, выбранному в качестве уникального идентификатора)
        for (T animal : aviary) {
            String nameAnimal = ((Animal) animal).getName();
            if (nameAnimal.hashCode() == name.hashCode() && nameAnimal.equals(name)) return animal;
        }
        return null;
    }

}
