package animals;

import food.Food;


public abstract class Animal {

    protected String name;
    protected double weight;
    protected int age;

    public void getAll() {
        System.out.println(getName() + ", весит " + getWeight() + " кг и ему " + getAge() + " лет.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract boolean eat(Food food);
}
