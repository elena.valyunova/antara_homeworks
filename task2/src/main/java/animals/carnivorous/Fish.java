package animals.carnivorous;

import animals.Interfaces.Swim;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name, double weight, int age) {
        super(name, weight, age);
    }

    public void swim() {
        System.out.println("Рыбка поплыла");
    }
}
