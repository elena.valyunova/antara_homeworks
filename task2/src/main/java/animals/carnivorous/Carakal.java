package animals.carnivorous;

import animals.Interfaces.Run;
import animals.Interfaces.Voice;

public class Carakal extends Carnivorous implements Run, Voice {

    public Carakal(String name, double weight, int age) {
        super(name, weight, age);
    }

    public String voice() {
        return "Мя мя";
    }

    public void run() {
        System.out.println("Каракал побежал");
    }

}
