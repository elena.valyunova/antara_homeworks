package animals.carnivorous;

import animals.Interfaces.Fly;
import animals.Interfaces.Voice;

public class Eagle extends Carnivorous implements Fly, Voice {

    public Eagle(String name, double weight, int age) {
        super(name, weight, age);
    }

    public String voice() {
        return "Кли-кли-кли-кли";
    }

    public void fly() {
        System.out.println("Орел полетел");
    }

}
