package animals.carnivorous;

import animals.Animal;
import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name, double weight, int age) {
        super();
    }

    public boolean eat(Food food) {
        return food instanceof Meat;
    }
}
