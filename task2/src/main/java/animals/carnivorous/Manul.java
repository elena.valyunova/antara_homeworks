package animals.carnivorous;

import animals.Interfaces.Run;
import animals.Interfaces.Voice;

public class Manul extends Carnivorous implements Run, Voice {

    public Manul(String name, double weight, int age) {
        super(name, weight, age);
    }

    public String voice() {
        return "Мяу-мяу";
    }

    public void run() {
        System.out.println("Манул побежал");
    }
}
