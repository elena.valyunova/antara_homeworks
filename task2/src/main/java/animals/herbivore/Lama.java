package animals.herbivore;

import animals.Interfaces.Run;

public class Lama extends Herbivore implements Run {

    public Lama(String name, double weight, int age) {
        super(name, weight, age);
    }

    public void run() {
        System.out.println("Лама бежит");
    }

}
