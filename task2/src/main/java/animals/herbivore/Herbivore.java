package animals.herbivore;

import animals.Animal;
import food.Food;
import food.Grass;


public abstract class Herbivore extends Animal {
    public Herbivore(String name, double weight, int age) {
        super();
    }

    public boolean eat(Food food) {
        return food instanceof Grass;
    }
}
