package animals.herbivore;

import animals.Interfaces.Fly;
import animals.Interfaces.Swim;
import animals.Interfaces.Voice;

public class Duck extends Herbivore implements Fly, Swim, Voice {

    public Duck(String name, double weight, int age) {
        super(name, weight, age);
    }

    public void fly() {
        System.out.println("Уточка полетела");
    }

    public void swim() {
        System.out.println("Уточка поплыла");
    }

    public String voice() {
        return "Кря-кря";
    }
}
