package animals.herbivore;

import animals.Interfaces.Run;

public class Giraffe extends Herbivore implements Run {

    public Giraffe(String name, double weight, int age) {
        super(name, weight, age);
    }

    public void run() {
        System.out.println("Жираф бежит");
    }

}
