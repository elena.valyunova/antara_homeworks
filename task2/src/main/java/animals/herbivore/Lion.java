package animals.herbivore;

import animals.Interfaces.Run;
import animals.Interfaces.Voice;

public class Lion extends Herbivore implements Voice, Run {

    public Lion(String name, double weight, int age) {
        super(name, weight, age);
    }

    public String voice() {
        return "Это мой выбор - быть вегетарианцем";
    }


    public void run() {
        System.out.println("Лев бежит");
    }
}
