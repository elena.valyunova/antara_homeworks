import animals.Animal;
import animals.Interfaces.Swim;
import animals.Interfaces.Voice;
import animals.carnivorous.Carakal;
import animals.carnivorous.Eagle;
import animals.carnivorous.Fish;
import animals.carnivorous.Manul;
import animals.herbivore.Duck;
import animals.herbivore.Giraffe;
import animals.herbivore.Lama;
import animals.herbivore.Lion;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {

        Animal lama = new Lama("Alisa", 102.7, 5);
        Animal giraffe = new Giraffe("Melman", 1000, 14);
        Animal duck = new Duck("Kryakva", 1.1, 6);
        Animal lion = new Lion("Alex", 150, 7);
        Animal carakal = new Carakal("BigFloppa", 15, 10);
        Animal eagle = new Eagle("MightyEagle", 6.2, 16);
        Animal fish = new Fish("Bruce", 770, 8);
        Animal manul = new Manul("ElGato", 2.9, 7);
        Animal swanGirl = new Duck("Daisy", 8.5, 8);
        Animal swanBoy = new Duck("Dewey", 10, 9);

        lama.getAll();
        giraffe.getAll();
        duck.getAll();
        lion.getAll();
        carakal.getAll();
        eagle.getAll();
        fish.getAll();
        manul.getAll();
        swanBoy.getAll();
        swanGirl.getAll();

        Food meat = new Meat("meat");
        Food grass = new Grass("grass");

        Worker worker = new Worker("Mike");
        worker.feed(manul, grass);
        worker.feed(manul, meat);
        worker.getVoice((Voice) lion);
        //Выдаст ошибку:
        //worker.getVoice((Voice) fish);

        Swim[] pond = new Swim[3];
        pond[0] = (Swim) duck;
        pond[1] = (Swim) swanBoy;
        pond[2] = (Swim) swanGirl;

        for (Swim swim : pond) {
            swim.swim();
        }

    }
}
