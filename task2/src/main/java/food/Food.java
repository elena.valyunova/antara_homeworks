package food;

public abstract class Food {
    protected String title;

    public abstract String getTitle();

    public abstract void setTitle(String title);
}
