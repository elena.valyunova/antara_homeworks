import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik catOne = new Kotik(10, "Степан", 3, "Meow-meow", "grey", 10);
        Kotik catTwo = new Kotik();
        catTwo.setPrettiness(7);
        catTwo.setName("Мурзик");
        catTwo.setWeight(4);
        catTwo.setMeow("Meoooow");
        catTwo.setColor("ginger");
        catTwo.setEat(10);

        Kotik.meowComparison(catTwo.getMeow(), catOne.getMeow());

        catOne.liveAnotherDay();
        catTwo.liveAnotherDay();

        System.out.println("\nПервый котик: " + catOne.getName() + " , его цвет " + catOne.getColor() + ", он милый на " + catOne.getPrettiness() + " из 10."
                + " Кот весит " + catOne.getWeight() + " килограмма." + "Он сыт на " + catOne.getEat() + " единиц.");
        System.out.println("\nВторой котик: " + catTwo.getName() + " , его цвет " + catTwo.getColor() + ", он милый на " + catTwo.getPrettiness() + " из 10."
                + " Кот весит " + catTwo.getWeight() + " килограмма." + "Он сыт на " + catTwo.getEat() + " единиц.");

        Kotik.getCountOfInstance();
    }
}
