package model;

public class Kotik {
    private int prettiness;
    private String name;
    private int weight;
    private String meow;
    private String color;
    private int satiety;
    private static int count;

    public Kotik() {
        count++;
    }

    public Kotik(int prettiness, String name, int weight, String meow, String color, int satiety) {

        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        this.color = color;
        this.satiety = satiety;
        count++;
    }

    public static void getCountOfInstance() {
        System.out.println("Количество экземпляров класса Kotik = " + count);
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getEat() {
        return satiety;
    }

    public void setEat(int satiety) {
        this.satiety = satiety;
    }

    public boolean satietyTest(int sat) {
        if (sat < 1) {
            System.out.println("Покорми кота!");
            return false;
        } else return true;
    }

    public Boolean play() {
        if (!satietyTest(satiety)) return false;
        else {
            System.out.println("Вы поиграли с " + name);
            satiety = satiety - 2;
            return true;
        }
    }

    public Boolean sleep() {
        if (!satietyTest(satiety)) return false;
        else {
            System.out.println("Котик " + name + "  поспал");
            satiety--;
            return true;
        }
    }

    public Boolean chaseMouse() {
        if (!satietyTest(satiety)) return false;
        else {
            if (satiety / 2 == 0) {
                System.out.println("Котик " + name + " поймал мышку!");
            } else {
                System.out.println("Котик " + name + " ловил мышку, но она убежала");
                satiety--;
            }
            return true;
        }
    }

    public Boolean walk() {
        if (!satietyTest(satiety)) return false;
        else {
            System.out.println("Котик " + name + "  пошел на прогулку");
            satiety = satiety - 2;
            return true;
        }
    }

    public Boolean tearFurniture() {
        if (!satietyTest(satiety)) return false;
        else {
            System.out.println("Котик " + name + "  портит мебель");
            satiety--;
            return true;
        }
    }

    public Boolean mew() {
        if (!satietyTest(satiety)) return false;
        else {
            System.out.println(meow);
            satiety--;
            return true;
        }
    }

    public int eat(int sat) {
        satiety = satiety + sat;
        System.out.println("Котик " + name + " поел");
        return satiety;
    }

    public int eat(int sat, String eat) {
        satiety = sat + satiety;
        return satiety;
    }

    public void eat() {
        eat(2, "корм");
    }

    public static void meowComparison(String two, String one) {
        System.out.println(one.equals(two));
    }


    public void liveAnotherDay() {

        int H = 9;
        int val = 0;
        for (int i = 0; i < 24; i++) {
            int num = (int) (Math.random() * H + 1);
            switch (num) {
                case 1:
                    play();
                    break;
                case 2:
                    sleep();
                    break;
                case 3:
                    walk();
                    break;
                case 4:
                    tearFurniture();
                    break;
                case 5:
                    chaseMouse();
                    break;
                case 6:
                    eat();
                    break;
                case 7:
                    val = eat(2);
                    System.out.println("Сытость котика: " + val);
                    break;
                case 8:
                    val = eat(1, "рыба");
                    System.out.println("Сытость котика: " + val);
                    break;
                case 9:
                    mew();
                    break;
            }
        }
    }
}
