--1. Вывести список самолетов с кодами 320, 321, 733
SELECT FLIGHT_ID , FLIGHT_NO 
FROM FLIGHTS
WHERE FLIGHT_ID IN ('320','321','733')

--2. Вывести список самолетов с кодом не на 3;
SELECT FLIGHT_ID , FLIGHT_NO 
FROM FLIGHTS
WHERE FLIGHT_ID NOT LIKE '3%'

--3. Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;
SELECT TICKET_NO , PASSENGER_NAME , EMAIL 
FROM TICKETS 
WHERE PASSENGER_NAME LIKE 'OLGA%' AND (EMAIL LIKE 'olga%' OR EMAIL IS NULL )

--4. Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета
SELECT *
FROM AIRCRAFTS_DATA 
WHERE "RANGE" IN ('5600','5700')
ORDER BY "RANGE" DESC 

--5. Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию
SELECT CITY||' '||AIRPORT_NAME 
FROM AIRPORTS_DATA 
WHERE CITY ='Moscow'
ORDER BY AIRPORT_NAME 

--6. Вывести список всех городов без повторов в зоне «Europe»
SELECT DISTINCT  CITY , TIMEZONE 
FROM AIRPORTS_DATA 
WHERE TIMEZONE LIKE ('Europe%')
ORDER BY CITY 

--7. Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
SELECT  BOOK_REF , (TOTAL_AMOUNT*90 / 100) AS TOTAL_AMOUNT_10
FROM BOOKINGS 
WHERE BOOK_REF LIKE '3A4%' 

--8. Вывести все данные по местам в самолете с кодом 320 и классом «Business» строками вида 
--«Данные по месту: номер места 1», «Данные по месту: номер места 2» … и тд
SELECT AIRCRAFT_CODE ||' code, '||FARE_CONDITIONS||' class: seat number '||SEAT_NO 
FROM SEATS 
WHERE AIRCRAFT_CODE  LIKE '320' AND FARE_CONDITIONS = 'Business'
ORDER BY SEAT_NO 

--9. Найти максимальную и минимальную сумму бронирования в 2017 году
SELECT (MAX(TOTAL_AMOUNT)) AS Max_value, (MIN(TOTAL_AMOUNT)) AS Min_Value
FROM BOOKINGS 
WHERE TRUNC(BOOK_DATE,'yy')='01.01.2017'

--10. Найти количество мест во всех самолетах, вывести в разрезе самолетов
SELECT AIRCRAFT_CODE, COUNT(AIRCRAFT_CODE) 
FROM SEATS 
GROUP BY AIRCRAFT_CODE 

--11. Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест
SELECT AIRCRAFT_CODE, FARE_CONDITIONS , COUNT(AIRCRAFT_CODE) 
FROM SEATS
GROUP BY AIRCRAFT_CODE , FARE_CONDITIONS 
ORDER BY AIRCRAFT_CODE 

--12.  Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11
SELECT PASSENGER_NAME , COUNT(TICKET_NO) 
FROM TICKETS 
WHERE PASSENGER_NAME = 'ALEKSANDR STEPANOV' AND PHONE LIKE '%11'
GROUP  BY PASSENGER_NAME 

--13. Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. 
-- Отсортировать по убыванию количества билетов
SELECT PASSENGER_NAME , (COUNT(TICKET_NO)) AS Count_ticket_no
FROM TICKETS 
WHERE PASSENGER_NAME LIKE 'ALEKSANDR %'
GROUP BY PASSENGER_NAME HAVING COUNT(TICKET_NO) > 2000
ORDER BY Count_ticket_no DESC 

--14. Вывести дни в сентябре 2017 с количеством рейсов больше 500
SELECT TRUNC(DATE_DEPARTURE) AS DATE_DEPARTURE_Trunc , SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=9 THEN 1 ELSE 0 END) count_september 
FROM FLIGHTS  
WHERE TRUNC(DATE_DEPARTURE) BETWEEN '01.09.17' AND '30.09.17'
GROUP BY TRUNC(DATE_DEPARTURE) HAVING SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=9 THEN 1 ELSE 0 END) > 500
ORDER BY DATE_DEPARTURE_Trunc

--15. Вывести список городов, в которых несколько аэропортов
SELECT CITY 
FROM AIRPORTS_DATA ad 
GROUP BY CITY HAVING COUNT(city) > 1

--16. Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата
SELECT AIRCRAFT_CODE , LISTAGG(SEAT_NO , ', ') WITHIN GROUP (order by SEAT_NO) as seats_list
FROM SEATS 
GROUP BY AIRCRAFT_CODE 

--17. Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017
SELECT FLIGHT_ID ,FLIGHT_NO ,DEPARTURE_AIRPORT ,DATE_DEPARTURE 
FROM FLIGHTS 
WHERE TRUNC(DATE_DEPARTURE, 'mm') ='01.09.2017'  AND DEPARTURE_AIRPORT IN  ('VKO','SVO','DME')

--18. Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017
SELECT DEPARTURE_AIRPORT , (COUNT(DEPARTURE_AIRPORT)) AS count_flights
FROM FLIGHTS 
WHERE TRUNC(DATE_DEPARTURE, 'yy') ='01.01.2017'  AND DEPARTURE_AIRPORT IN  ('VKO','SVO','DME')
GROUP BY DEPARTURE_AIRPORT 

--19. Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017
SELECT DEPARTURE_AIRPORT , 
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=1 THEN 1 ELSE 0 END) January,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=2 THEN 1 ELSE 0 END) February,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=3 THEN 1 ELSE 0 END) March,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=4 THEN 1 ELSE 0 END) April,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=5 THEN 1 ELSE 0 END) May,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=6 THEN 1 ELSE 0 END) June,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=7 THEN 1 ELSE 0 END) July,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=8 THEN 1 ELSE 0 END) August,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=9 THEN 1 ELSE 0 END) September,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=10 THEN 1 ELSE 0 END) October,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=11 THEN 1 ELSE 0 END) November,
	SUM(CASE WHEN to_char (DATE_DEPARTURE,'mm')=12 THEN 1 ELSE 0 END) December
FROM FLIGHTS 
WHERE DEPARTURE_AIRPORT IN  ('VKO','SVO','DME') AND TRUNC(DATE_DEPARTURE) BETWEEN '01.01.17' AND '31.12.17'
GROUP BY DEPARTURE_AIRPORT

--20. Найти все билеты по бронированию на «3A4B»
SELECT BOOK_REF , TICKET_NO 
FROM TICKETS  
WHERE BOOK_REF LIKE '3A4B%'

--21. Найти все перелеты по бронированию на «3A4B»
SELECT t.BOOK_REF , tf.FLIGHT_ID 
FROM TICKETS t , TICKET_FLIGHTS tf  
WHERE t.TICKET_NO = tf.TICKET_NO AND t.BOOK_REF LIKE '3A4B%'
















