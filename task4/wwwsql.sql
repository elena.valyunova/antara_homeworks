--Задание: 1
select model, speed, hd
from PC
where price < 500

--Задание: 2 
Select distinct maker
from Product 
where type = 'Printer'

--Задание: 3 
select model, ram, screen
from laptop
where price>1000

--Задание: 4
Select *
from printer
where color = 'y'

--Задание: 5
Select model, speed, hd
from PC
where cd in ('12x','24x') and price<600

--Задание: 6
Select distinct p.maker , l.speed
from Product p , Laptop l
where l.hd>=10 and p.model = l.model

--Задание: 7
Select distinct product.model, pc.price
from Product JOIN PC on product.model = pc.model where maker = 'B'
UNION
Select distinct product.model, laptop.price
from product JOIN Laptop on product.model=laptop.model where maker='B'
UNION
Select distinct product.model, printer.price
from product JOIN Printer on product.model=printer.model where maker='B'

--Задание: 8
Select maker
from Product
where type = 'PC'
except
Select maker
from Product
where type = 'Laptop'

--Задание: 9
Select distinct maker
from Product p, PC pc
where pc.model = p.model and speed >=450

--Задание: 10
Select model, price
from Printer
where price = 
(Select MAX(price)
from Printer )

--Задание: 11
Select avg(speed)
from PC

--Задание: 12
Select avg(speed)
from Laptop
where price>1000

--Задание: 13
Select avg(pc.speed)
from PC pc, Product p
where pc.model = p.model and maker ='A'

--Задание: 14
Select s.class, s.name, c.country
from Ships s, Classes c
where s.class = c.class and numGuns >=10

--Задание: 15
Select hd
from PC
group by hd having count(model)>=2

--Задание: 16
Select distinct pc1.model, pc2.model, pc1.speed, pc1.ram
from PC pc1, PC pc2
where pc1.speed = pc2.speed and pc1.ram = pc2.ram and pc1.model>pc2.model

--Задание: 17
Select distinct p.type, l.model, l.speed
from PC pc, Laptop l, Product p
where l.model = p.model and l.speed< (select min(speed) from pc)

--Задание: 18
Select distinct pd.maker, pt.price
from product pd, printer pt
where pd.model = pt.model
and pt.color = 'y'
and pt.price = (select MIN(price) FROM printer where color = 'y')

--Задание: 19
Select p.maker , avg(l.screen)
from Product p, Laptop l
where p.model=l.model
group by p.maker

--Задание: 20
Select maker, count(model)
from Product 
where type = 'PC'
group by maker having count(distinct model)>=3

--Задание: 21 
Select p.maker, max(price)
From PC pc, Product p
where pc.model = p.model
group by p.maker